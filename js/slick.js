$('.farmacias .slider').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    centerMode: true,
    centerPadding: '38px',
    arrows: false,
    dots: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 703,
        settings: {
          slidesToShow: 3,
          centerMode: false,
        }
      },
      {
        breakpoint: 508,
        settings: {
          slidesToShow: 3,
          centerMode: false,
        }
      }
      
    ]
  })


  $('.depoimentos .content .slider-depoimentos').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: false,
  })



  $('.planos-home .conteiner .content-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    arrows: true,
    dots: false,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>',
    responsive: [
        {
          breakpoint: 1197,
          settings: {
            slidesToShow: 2,
          }
        },
        {
            breakpoint: 800,
            settings:{
                slidesToShow: 1,
                arrows: false,
                dots: true,
            }
            
        }
    ]   
});